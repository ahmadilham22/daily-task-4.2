class Vehicle {
  constructor(type, productionCountry) {
    this.type = type;
    this.productionCountry = productionCountry;
  }

  info() {
    console.log(
      `Jenis kendaran roda ${this.type} dari negara ${this.productionCountry}`
    );
  }
}
class Mobil extends Vehicle {
  constructor(type, productionCountry, brand, price, tax) {
    super(type, productionCountry);
    this.brand = brand;
    this.price = price;
    this.tax = tax;
  }
  totalPrice() {
    let carTax = this.price + this.price * (this.tax / 100);
    return carTax;
  }

  info() {
    super.info();
    console.log(
      `Kendaraan Ini Mereknya ${this.brand} dengan harga ${this.totalPrice()}`
    );
  }
}

const kendaraan = new Vehicle(2, "Jepang");
kendaraan.info();
const mobil = new Mobil(4, "Jerman", "Mercedes", 800000000, 10);
mobil.info();
